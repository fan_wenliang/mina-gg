Page({
  data: {
    tabs: ['全部', '待确认', '处理中', '已完成'],
    activeIndex: 0,
    underlineLeft: 0,
    underlineWidth: 25, // 假设每个标签占据25%的宽度
    underlineStyle: 'left: 0%; width: 25%;',
    searchQuery: '',
    orderList: [
      {
        orderNumber: 'CN2407220000116',
        orderTime: '2024-03-10',
        orderStatus: '已完成',
        orderType: '引物合成'
      },
      {
        orderNumber: 'CN2407190000385',
        orderTime: '2024-07-11',
        orderStatus: '处理中',
        orderType: 'Sanger测序'
      },
      {
        orderNumber: 'CN2407220000112',
        orderTime: '2024-04-19',
        orderStatus: '待确认',
        orderType: '引物合成'
      },
      {
        orderNumber: 'CN2407190000375',
        orderTime: '2024-06-11',
        orderStatus: '处理中',
        orderType: '基因合成'
      }
    ],
    filteredOrderList: []
  },

  onLoad: function() {
    // 初始加载时显示全部订单
    this.filterOrders(0);
  },

  activateTab: function(event) {
    const index = event.currentTarget.dataset.index;
    const underlineLeft = index * this.data.underlineWidth;
    const underlineWidth = this.data.underlineWidth;

    this.setData({
      activeIndex: index,
      underlineLeft: underlineLeft,
      underlineStyle: `left: ${underlineLeft}%; width: ${underlineWidth}%;`
    });

    // 根据当前选中的标签过滤订单
    this.filterOrders(index);
  },

  onSearchInput: function(event) {
    const query = event.detail.value.toLowerCase();
    this.setData({
      searchQuery: query
    });
    this.filterOrders(this.data.activeIndex);
  },

  filterOrders: function(index) {
    let filteredOrders = [];

    if (index === 0) {
      // 显示全部订单
      filteredOrders = this.data.orderList;
    } else {
      // 根据标签过滤订单
      const status = this.data.tabs[index];
      filteredOrders = this.data.orderList.filter(order => order.orderStatus === status);
    }

    if (this.data.searchQuery) {
      // 根据搜索输入过滤订单号
      filteredOrders = filteredOrders.filter(order => order.orderNumber.toLowerCase().includes(this.data.searchQuery));
    }

    this.setData({
      filteredOrderList: filteredOrders
    });
  }
});
